/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fai.utb.algorithms.winnowing;

/**
 * @author Radek
 */
public class Winnowing {

    /**
     * Create array of hashes
     *
     * @param ngrams generated n-gram strings
     * @return array of hash values from n-gram
     */
    public int[] createHash(String[] ngrams) {
        int[] output = new int[ngrams.length];

        int index = 0;
        for (String ngram : ngrams) {
            output[index++] = ngram.hashCode();
        }
        return output;
    }

    /**
     * Generates Winnowing fingerprint of document. Fingerprint is based on min
     * value from each window.
     *
     * @param input array of k-gram hashes
     * @param w window length
     * @return
     */
    public WinnowFingerPrint fingerPrintFromHashArray(int[] input, int w) {
        WinnowFingerPrint fp = new WinnowFingerPrint();
        int n = input.length;
        int lastMin = -1;

        for (int i = 0; i < n - w + 1; i++) {
            int min = Integer.MAX_VALUE;
            int index = 0;
            for (int j = 0; j < w; j++) {
                if (input[i + j] < min) {
                    min = input[i + j];
                    index = i + j;
                }
            }

            if (lastMin != index) {
                fp.putHash(index, min);
                lastMin = index;
            }

        }
        return fp;
    }

    /**
     * Generates WinnowFingerPrint
     * 
     * @param ngrams
     * @param windowLength
     * @return 
     */
    public WinnowFingerPrint createFingerPrint(String[] ngrams, int windowLength) {
        int[] hashes = createHash(ngrams);
        return fingerPrintFromHashArray(hashes, windowLength);
    }

}
