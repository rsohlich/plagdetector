/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service.impl;

import cz.sohlich.app.repository.ClusterRepository;
import cz.sohlich.app.service.SimilarityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author radek
 */
@Service
public class SimilarityServiceImpl implements SimilarityService {
    @Autowired ClusterRepository clusterRepo;
    
    
    
    @Override
    public Double getSubmissionInfo(String submissionId) {
        return clusterRepo.findMaxSimilarityBySubmission(submissionId);
    }
    
    @Override
    public List getAssignmentSimilaritiesInfo(String assignmentId) {
         return  clusterRepo.findMaxSimilarityForSubmissions(assignmentId);
    }
}
