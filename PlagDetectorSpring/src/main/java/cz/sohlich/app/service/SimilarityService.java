/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service;

import java.util.List;

/**
 *
 * @author radek
 */
public interface SimilarityService {

    
    List getAssignmentSimilaritiesInfo(String assignmentId);

    
    Double getSubmissionInfo(String submissionId);
    
}
