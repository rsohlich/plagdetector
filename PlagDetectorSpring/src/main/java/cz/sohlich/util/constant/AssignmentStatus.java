/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.util.constant;

/**
 *
 * @author Radek
 */
public abstract class AssignmentStatus {
    
    public static final String NEW = "NEW";
    public static final String SYNCED = "SYNCED";
    public static final String CLOSED = "CLOSED";
    
}
