/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fai.utb.py.lexer;


import cz.fai.utb.lang.lexer.LexerBase;
import cz.fai.utb.ngram.NGram;
import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenSource;
import org.antlr.v4.runtime.TokenStream;

/**
 *
 * @author Radek
 */
public class Python3LexerImpl extends LexerBase{


    //lexer splits input into tokens
    public TokenStream process(String inputString) {
        CharStream input = new PythonUnicodeInputStream(new ANTLRInputStream(inputString));
        TokenStream tokens = new CommonTokenStream(new Python3Lexer(input));
        return tokens;
    }

    //lexer splits input into tokens
//    public String generateAST(String inputString) {
//        CharStream input = new PythonUnicodeInputStream(new ANTLRInputStream(inputString));
//        TokenStream tokens = new CommonTokenStream(new Python3Lexer(input));
//
//        Python3Parser parser = new Python3Parser(tokens);
//        ParseTree tree = parser.compilationUnit();
//
//        return tree.toStringTree(parser);
//    }

    public String[] tokenize(String inputString) {
        TokenStream tokens = process(inputString);
        List<String> tokenList = new ArrayList<>();

        TokenSource source = tokens.getTokenSource();

        while (true) {
            Token t = source.nextToken();

            if (t.getText().contains("EOF")) {
                break;
            }
            
            int type = t.getType();
            switch (type) {
                case 13:
                case 14:
                case 15:
                    loopCount++;
                    break;
                case 93:
                    identifierCount++;
                    break;
                case 10:
                    ifCount++;
                    break;
            }
            
            tokenList.add(String.valueOf(type));
        }

        String[] stockArr = new String[tokenList.size()];
        stockArr = tokenList.toArray(stockArr);
        return stockArr;
    }

    public void nGramize(String inputString, int n) {
        String[] tokens = tokenize(inputString);

        for (int i = 0; i < tokens.length; i++) {
            NGram ngram = new NGram(n, tokens, i);
            nGrams.add(ngram.toString());
        }
    }  
}
