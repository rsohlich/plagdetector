/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.rest.controller;

import cz.sohlich.app.api.SubmissionRest;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import cz.sohlich.app.GenericTest;
import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dao.File;
import cz.sohlich.app.repository.AssignmentRepository;
import cz.sohlich.app.repository.FileRepository;
import cz.sohlich.app.service.PluginService;
import cz.sohlich.app.service.SubmissionHelper;
import cz.sohlich.util.FileHandler;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author radek
 */
public class SubmissionRestTest extends GenericTest {

    @Mock AssignmentRepository assignmentRepo;
    @Mock FileRepository fileRepo;
    @Mock SubmissionHelper submissionHelper;
    @Mock PluginService pluginHolder;

    @InjectMocks SubmissionRest submissionRest;

    public SubmissionRestTest() {
    }

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(submissionRest).build();
        RestAssuredMockMvc.mockMvc = mockMvc;
        presetBeans();
    }

    public void presetBeans() throws IOException {
        try {folder.newFile("62.zip");} catch (IOException e) {System.out.println("Zip already exists."); }

        Assignment returnEntity = new Assignment("rada", "C", "Test");
        File returnFileEntity = new File("123", returnEntity.getAssignmentId(), UUID.randomUUID().toString(), "radek", new byte[0], new Date());
        //Preset the return values
        Mockito.when(pluginHolder.isSupportedLang(anyString())).thenReturn(Boolean.TRUE);
        Mockito.when(assignmentRepo.findOneByAssignmentId(anyString())).thenReturn(Optional.of(returnEntity));
        Mockito.when(fileRepo.findByOwner(anyString())).thenReturn(Arrays.asList(returnFileEntity));
        Mockito.doNothing().when(submissionHelper).process(any(FileHandler.class), any(Assignment.class), anyString(), anyString());
        Mockito.when(assignmentRepo.save(any(Assignment.class))).thenReturn(returnEntity);
        submissionRest.setTempFolder(folder.newFolder().getAbsolutePath());
    }

    @Test
    public void testCreateSubmission() {
        given().
                multiPart("submission-data", "62.zip", new byte[0], "application/zip").
                formParam("submission-meta", "{ \"owner\": \"rada\", \"assignmentId\":\"54f205ff3ef60a124d826699\", \"identificator\": \"62\" }", "multipart/form-data").
                when().
                post("/rest/submission/create").
                then().
                statusCode(HttpServletResponse.SC_OK);
    }

    @Test
    public void testCreateSubmissionFileExist() {
        given().
                contentType("application/json").
                body("{ \"owner\": \"rada\", \"assignmentId\":\"54f205ff3ef60a124d826699\", \"identificator\": \"62\" }").
                when().
                put("/rest/submission/create").
                then().
                log().all().
                statusCode(HttpServletResponse.SC_OK);
    }

}
