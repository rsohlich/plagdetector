/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.entity.dao;

import java.util.Date;
import java.util.Map;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Radek
 */
public class File {

    @Id
    private String fileId;

    String filename;
    
    @Indexed
    String assignmentId;
    
    @Indexed
    String submissionId;
    
    String clusterId;

    String owner;

    byte[] file;

    boolean suspected = false;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    Date createdOn = new Date();

    Map footprint;
    Integer[] sequence;
    Integer[] indexSequence;
    Boolean template;

    double[] clusterAttributes = {0, 0, 0};

    public File() {
    }

    public File(String fileId, String assignmentId, String submissionId, String owner, byte[] file, Date createdOn) {
        this.fileId = fileId;
        this.assignmentId = assignmentId;
        this.submissionId = submissionId;
        this.owner = owner;
        this.file = file;
        this.createdOn = createdOn;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getAssignment() {
        return assignmentId;
    }

    public void setAssignment(String assignment) {
        this.assignmentId = assignment;
    }

    public String getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(String submissionId) {
        this.submissionId = submissionId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Map getFootprint() {
        return footprint;
    }

    public void setFootprint(Map footprint) {
        this.footprint = footprint;
    }

    public Integer[] getSequence() {
        return sequence;
    }

    public void setSequence(Integer[] sequence) {
        this.sequence = sequence;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public boolean isSuspected() {
        return suspected;
    }

    public void setSuspected(boolean suspected) {
        this.suspected = suspected;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Boolean isTemplate() {
        return template;
    }

    public void setTemplate(Boolean template) {
        this.template = template;
    }

    public double[] getClusterAttributes() {
        return clusterAttributes;
    }

    public void setClusterAttributes(double[] clusterAttributes) {
        this.clusterAttributes = clusterAttributes;
    }

    public String getClusterId() {
        return clusterId;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }

    public Integer[] getIndexSequence() {
        return indexSequence;
    }

    public void setIndexSequence(Integer[] indexSequence) {
        this.indexSequence = indexSequence;
    }

}
