/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service.impl;

import cz.fai.utb.algorithms.gst.GreedyStringTiling;
import cz.fai.utb.algorithms.gst.Match;
import cz.fai.utb.data.dto.SimplifiedFileDTO;
import cz.fai.utb.match.jaccard.Jaccard;
import cz.sohlich.app.integration.apac.ApacClient;
import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dao.CompareResult;
import cz.sohlich.app.entity.dao.File;
import cz.sohlich.app.repository.AssignmentRepository;
import cz.sohlich.app.repository.ClusterRepository;
import cz.sohlich.app.repository.ComparisonRepository;
import cz.sohlich.app.repository.FileRepository;
import cz.sohlich.app.repository.impl.FileRepositoryImpl;
import cz.sohlich.app.service.ClusterService;
import cz.sohlich.app.service.EvaluatorService;
import cz.sohlich.util.constant.AssignmentStatus;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.StreamSupport;
import org.apache.commons.math3.util.Combinations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Radek
 */
@Component
public class ScheduledEvaluatorImpl implements EvaluatorService {

    final Logger logger = LoggerFactory.getLogger("ScheduledEvaluator");

    @Autowired AssignmentRepository assignmentRepo;
    @Autowired FileRepository fileRepo;
    @Autowired ClusterService clusterProcessor;
    @Autowired ClusterRepository clusterRepository;
    @Autowired ComparisonRepository comparisonRepository;
    @Autowired ApacClient apacClient;

    @Value("${pg.cluster.treshold}") Double treshold;
    @Value("${pg.gst.minlength}") Integer gstMinLenght;

    final GreedyStringTiling gstProcessor = new GreedyStringTiling();

    @Scheduled(cron = "0 */1 * * * *")
    @Override
    public void processFiles() {

        logger.info("Zacinam zpracovani souboru");

        final List<Assignment> assignments = assignmentRepo.findByStatus(AssignmentStatus.NEW);

        logger.info("{} assignments to be processed", assignments.size());

        assignments.stream().forEach((assignment) -> {
            try {
                comparisonRepository.deleteByAssignment(assignment.getAssignmentId());
                List<CompareResult> similarFiles = fileRepo.getSimilarFiles(assignment.getAssignmentId());
                comparisonRepository.save(similarFiles);
                
                
                List<File> assignedFiles = fileRepo.findByAssignmentId(assignment.getAssignmentId());

                if (!assignedFiles.isEmpty()) {
                    logger.info("Assignment {} number of {} to be porcessed", assignment.getAssignmentId(), assignedFiles.size());
                    Combinations combination = new Combinations(assignedFiles.size(), 2);

                    //Generate combinations
                    StreamSupport.stream(combination.spliterator(), true).forEach((tuple) -> {
                        File f1 = assignedFiles.get(tuple[0]);
                        File f2 = assignedFiles.get(tuple[1]);

                        if (!Objects.equals(f1.getFileId(), f2.getFileId())
                                && !f1.getOwner().equals(f2.getOwner()) && comparisonRepository.findByFile(new String[]{f1.getFileId(), f2.getFileId()}) == null) {

                            double result = Jaccard.compute(f1.getFootprint(), f2.getFootprint());
                            if (result > treshold) {
                                f1.setSuspected(true);
                                f2.setSuspected(true);
                                fileRepo.save(Arrays.asList(f1, f2));
                            }

                            List<Match> matches = null;
//                            if (result >= treshold) {
//                                matches = gstProcessor.process(f1.getSequence(), f2.getSequence(), gstMinLenght);
//                                result = gstProcessor.computeSimilarity(matches, f1.getSequence().length, f2.getSequence().length);
//                            }
                            comparisonRepository.save(new CompareResult(f1, f2, result, matches, assignment.getAssignmentId()));
                        }
                    });

                } else {
                    logger.info("No file for assignment {0}", assignment.toString());
                }
            } catch (Exception ex) {
                logger.error("Process file", ex);
            }

            //Sync all similarities
            boolean isSynced = apacClient.synchronize(assignment.getAssignmentId());
            assignment.setStatus(isSynced ? AssignmentStatus.SYNCED : AssignmentStatus.NEW);
            assignmentRepo.save(assignment);
        });

    }

}
