<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="myApp" class="no-js"> <!--<![endif]-->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Navbar with Search</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/angular-chart.css" rel="stylesheet">
        <!--[if lt IE 9]>
                <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href="css/styles.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" rel="home" href="#">PlagDetector</a>
            </div>

            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav">
                    <li><a href="#/view1">DashBoard</a></li>
<!--                    <li>
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                Dropdown
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </li>-->
                </ul>
                <div class="col-sm-2 col-md-2 pull-right" >
                    <div class="navbar-text">
                        <a href="#">Inbox <span class="badge">42</span>

                        </a>
                    </div>
                </div>
                <div class="col-sm-2 col-md-2 pull-right">
                    <div class="glyphicon glyphicon-user navbar-text"></div><div class="navbar-text"></div>
                    <!--		<form class="navbar-form" role="search">
                                    <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
                                            <div class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                            </div>
                                    </div>
                                    </form>-->
                </div>

            </div>
        </div>


        <div ng-view class="container">


        </div>

        <!-- In production use:
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/x.x.x/angular.min.js"></script>
        -->
        <script src="js/bootstrap/jquery-1.11.2.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/chart/Chart.min.js"></script>
        <script src="js/angular/angular.js"></script>
        <script src="js/angular/angular-route.js"></script>
        <script src="js/angular/angular-resource.min.js"></script>
        <script src="js/chart/angular-chart.js"></script>
        <script src="js/services.js"></script>
        <script src="js/app.js"></script>
        <script src="js/view/dashboard.js"></script>
        <script src="js/view/view2.js"></script>
        <script src="js/version/version.js"></script>
        <script src="js/version/version-directive.js"></script>
        <script src="js/version/interpolate-filter.js"></script>
    </body>
</html>
