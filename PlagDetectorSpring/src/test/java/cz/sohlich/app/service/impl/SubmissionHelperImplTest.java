/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service.impl;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import cz.fai.utb.c.api.CProcessor;
import cz.sohlich.app.GenericTest;
import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dao.File;
import cz.sohlich.app.repository.FileRepository;
import cz.sohlich.app.service.PluginService;
import cz.sohlich.util.FileHandler;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author radek
 */
public class SubmissionHelperImplTest extends GenericTest {

    @Mock FileRepository fileRepo;
    @Mock PluginService pluginHolder;
    @InjectMocks SubmissionHelperImpl submissionHelper;

    //Test properties
    Assignment testAssignment;
    String destinationPath = System.getProperty("pg.filesystem.assignment.path");
    String uploadedSubmissionTempFolder;
    String testSubmissionName;
    File lastResult;
    

    public SubmissionHelperImplTest() {
        testAssignment = new Assignment("test_lector", "C", "test_assignment");
        testAssignment.setAssignmentId("test_id");
        testSubmissionName = "test_submission";

        try {
            URL submissionZipURL = ClassLoader.getSystemResource("test");
            java.io.File submissionFolder = new java.io.File(submissionZipURL.toURI());
            uploadedSubmissionTempFolder = submissionFolder.getPath() + java.io.File.separator;
        } catch (Exception e) {
            System.err.println("Cannot load test file");
        }
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(submissionHelper).build();
        RestAssuredMockMvc.mockMvc = mockMvc;
        presetBeans();
    }

    /**
     * Preset mock bean methods
     */
    public void presetBeans() {

        Mockito.when(pluginHolder.getProcessorByLanguage(Matchers.anyString())).thenReturn(new CProcessor());
        
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of process method, of class SubmissionHelperImpl.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessUploadedFile() throws Exception {
        
        final List<File> savedFiles = new ArrayList<>();
        
        Mockito.when(fileRepo.save(Matchers.any(File.class))).thenAnswer((InvocationOnMock invocation) -> {
            savedFiles.add((File)invocation.getArguments()[0]);
            return new File();
        });
        
        
        FileHandler fileHandler = new FileHandler(destinationPath, uploadedSubmissionTempFolder, testSubmissionName);
        submissionHelper.process(fileHandler, testAssignment, "test_user", null);
        assertTrue(savedFiles.size() > 0);
    }

}
