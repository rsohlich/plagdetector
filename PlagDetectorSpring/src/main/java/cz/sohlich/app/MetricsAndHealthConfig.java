/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app;

/**
 *
 * @author radek
 */
import org.springframework.context.annotation.Configuration;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;
import cz.sohlich.app.health.ApplicationHealthCheck;
import cz.sohlich.app.health.DiskUsageHealthCheck;
import cz.sohlich.app.health.MongoHealthCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@EnableMetrics
public class MetricsAndHealthConfig extends MetricsConfigurerAdapter {

    @Autowired MongoTemplate mongoTemplate;

    @Override
    public void configureReporters(MetricRegistry metricRegistry) {
//        Slf4jReporter
//                .forRegistry(metricRegistry)
//                .build()
//                .start(1, TimeUnit.MINUTES);
    }

    @Bean(name = "metric-registry")
    public MetricRegistry metricRegistry() {
        MetricRegistry registry = new MetricRegistry();
        registry.registerAll(new GarbageCollectorMetricSet());
        registry.registerAll(new ThreadStatesGaugeSet());

        return registry;
    }

    @Bean(name = "health-check-registry")
    public HealthCheckRegistry healthCheckRegistry() {
        HealthCheckRegistry registry = new HealthCheckRegistry();
        registry.register("DB", new MongoHealthCheck(mongoTemplate));
        registry.register("Application", new ApplicationHealthCheck());
        registry.register("Disk", new DiskUsageHealthCheck());
        return registry;
    }

}
