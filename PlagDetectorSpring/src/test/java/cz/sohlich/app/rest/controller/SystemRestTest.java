/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.rest.controller;

import cz.sohlich.app.api.SystemRest;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import cz.sohlich.app.GenericTest;
import cz.sohlich.app.service.PluginService;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import static org.hamcrest.Matchers.containsString;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author radek
 */
public class SystemRestTest extends GenericTest {

    @Mock PluginService holder; //Mocks pluginholder
    @InjectMocks SystemRest rest;

    public SystemRestTest() throws IOException {
        super();
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(rest).build();
        RestAssuredMockMvc.mockMvc = mockMvc;
        
        presetBeans();
    }

    /**
     * Preset mock bean methods
     */
    public void presetBeans() {
        //Preset the return valuees
        Set returnVal = new HashSet();
        returnVal.addAll(Arrays.asList("C", "JAVA", "PYTHON"));
        Mockito.when(holder.getSupportedLangs()).thenReturn(returnVal);
        Mockito.doNothing().when(holder).load();
    }

    /**
     * Test of supportedLangs method, of class SystemRest.
     */
    @Test
    public void testSupportedLangs() {
        given().
                when().
                get("/system/langs").
                then().
                statusCode(HttpServletResponse.SC_OK)
                .body(containsString("[\"JAVA\",\"C\",\"PYTHON\"]"));
    }

}
