/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.entity.dto;

import cz.sohlich.app.entity.dao.Assignment;
import java.util.Date;

/**
 *
 * @author Radek
 */
public class AssignmentDTO {

    String assignmentId;
    String owner;
    String lang;
    String name;
    Date createdOn;

    public AssignmentDTO(Assignment assignment) {
        this.assignmentId = assignment.getAssignmentId();
        this.createdOn = assignment.getCreatedOn();
        this.lang = assignment.getLang();
        this.name = assignment.getName();
        this.owner = assignment.getOwner();
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    

}
