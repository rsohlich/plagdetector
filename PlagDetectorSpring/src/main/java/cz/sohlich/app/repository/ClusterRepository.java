/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository;

import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dao.Cluster;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Radek
 */
@Repository
public interface ClusterRepository extends MongoRepository<Cluster, String>,ClusterRepositoryCustom {

    public List<Cluster> deleteByAssignment(String assignment);
}
