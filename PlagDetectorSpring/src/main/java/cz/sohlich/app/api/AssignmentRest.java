/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.api;

import com.wordnik.swagger.annotations.ApiOperation;
import cz.fai.utb.data.dto.SimplifiedFileDTO;
import cz.sohlich.app.entity.dao.Assignment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import cz.sohlich.app.entity.dto.AssignmentDTO;
import cz.sohlich.app.entity.dto.DashboardStatItemDTO;
import cz.sohlich.app.repository.AssignmentRepository;
import cz.sohlich.app.repository.FileRepository;
import cz.sohlich.app.service.PluginService;
import cz.sohlich.app.api.dto.RestAssignmentDTO;
import cz.sohlich.app.exception.LanguageUnsupportedException;
import cz.sohlich.app.exception.AssignmentNotFoundException;
import cz.sohlich.app.service.SubmissionHelper;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Radek
 */
@RestController
@RequestMapping("/rest/assignment")
public class AssignmentRest {

    Logger log = LoggerFactory.getLogger(AssignmentRest.class);
    
    @Autowired AssignmentRepository assignmentRepo;
    @Autowired FileRepository fileRepo;
    @Autowired SubmissionHelper submissionHelper;
    @Autowired PluginService pluginHolder;

    @ApiOperation(value = "All assignments by owner")
    @RequestMapping(value = "/all/{owner}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AssignmentDTO> getAssignmentsByOwner(@PathVariable("owner") String owner) {
        //Namapovani Entity na DTO
        List<AssignmentDTO> assignmentList = StreamSupport.stream(assignmentRepo.findByOwner(owner).spliterator(), false)
                .map(AssignmentDTO::new)
                .collect(Collectors.toList());
        return assignmentList;
    }

    @ApiOperation(value = "Create assignment")
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public AssignmentDTO create(@RequestBody RestAssignmentDTO assignment) throws LanguageUnsupportedException {
        log.info("Creating ASSIGNMENT {}",assignment.toString());
        
        if(!pluginHolder.isSupportedLang(assignment.getLang()))throw new LanguageUnsupportedException();
        
        Assignment createdAssignment = new Assignment(assignment.getOwner(), assignment.getLang(), assignment.getName());
        createdAssignment.setCreatedNow();
        createdAssignment = assignmentRepo.save(createdAssignment);
        AssignmentDTO returnDto = new AssignmentDTO(createdAssignment);
        return returnDto;
    }

    
    @ApiOperation(value = "Delete assignment")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AssignmentDTO delete(@RequestParam("id") String id) throws AssignmentNotFoundException {
        Assignment existingAssignment = assignmentRepo.findOneByAssignmentId(id).orElseThrow(AssignmentNotFoundException::new);
        assignmentRepo.delete(id);
        return new AssignmentDTO(existingAssignment);
    }

    
    @ApiOperation(value = "Simple dashboard for owner")
    @RequestMapping(value = "/dashboard/{owner}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DashboardStatItemDTO statistics(@PathVariable("owner") String owner) {

        List<Assignment> assignments = assignmentRepo.findByOwner(owner);

        List submissionStat = fileRepo.getSubmissionStatisticsByAssignments(assignments);

        List suspectedStat = fileRepo.getSuspectedCountByAssignments(assignments);

        DashboardStatItemDTO dashboard = new DashboardStatItemDTO();
        dashboard.setAssignments(assignments);
        dashboard.setSubmissions(submissionStat);
        dashboard.setSuspecteds(suspectedStat);
        return dashboard;
    }
    
    @ApiOperation(value = "Returns all similarities for one assignment")
    @RequestMapping(value = "/one/{assignment}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SimplifiedFileDTO> getOneSimplified(@PathVariable("assignment") String assignmentId) {
        
      return  fileRepo.getClusterableFileByAssignment(assignmentId);
        
    }
    
    
    
    

}
