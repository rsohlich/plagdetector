/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository;

import cz.sohlich.app.entity.dto.DashboardStatItemDTO;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

/**
 *
 * @author Radek
 */
public interface AssignmentRepositoryCustom {
    public List<DashboardStatItemDTO> getStatiscticsByUserAndDate(String owner);
}
