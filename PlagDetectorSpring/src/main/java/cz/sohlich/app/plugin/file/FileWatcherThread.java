/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.plugin.file;

import cz.sohlich.app.plugin.event.PluginLoadedEvent;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;

/**
 *
 * @author radek
 */
public class FileWatcherThread implements Runnable {

    Logger log = LoggerFactory.getLogger(FileWatcherThread.class);
    private final Map<WatchKey, Path> keys;
    private final WatchService watcher;
    private final ApplicationEventPublisher eventPublisher;

    public FileWatcherThread(WatchService watcher, Map<WatchKey, Path> keys, ApplicationEventPublisher publisher) {
        this.watcher = watcher;
        this.keys = keys;
        this.eventPublisher = publisher;
    }

    @Override
    public void run() {
        log.info("File monitor waiting for event");

        // wait for key to be signalled
        WatchKey key = null;
        try {
            key = watcher.take();
        } catch (InterruptedException ex) {
            log.error("FileWatcher", ex);
        }

        Path dir = keys.get(key);
        if (dir == null) {
            log.info("Watchkey not recognized {}", key.toString());
            return;
        }

        for (WatchEvent<?> event : key.pollEvents()) {

            // Context for directory entry event is the file name of entry
            WatchEvent<Path> ev = (WatchEvent<Path>) event;
            Path name = ev.context();
            Path child = dir.resolve(name);

            //Read and import file
            String path = child.toString();
            log.info("New plugin added {}", path);
        }

        eventPublisher.publishEvent(new PluginLoadedEvent("Plugins loaded"));

        // reset key
        key.reset();
    }

}
