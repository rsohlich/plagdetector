/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app;

import com.mongodb.Mongo;
import cz.sohlich.app.mongo.MongoConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * @author Radek
 */
@Configuration
@EnableMongoRepositories
public class MongoConfig extends AbstractMongoConfiguration {

    @Autowired
    MongoConnectionFactory mongo;

    
    
    
    @Override
    protected String getDatabaseName() {
        return mongo.getDB();
    }

    @Override
    public Mongo mongo() throws Exception {
        return mongo.createConnection();
    }

    @Bean
    @Override
    public MongoTemplate mongoTemplate() throws Exception {
      return new MongoTemplate(mongo.createConnection(), mongo.getDB());
    }
    
    
    
}
