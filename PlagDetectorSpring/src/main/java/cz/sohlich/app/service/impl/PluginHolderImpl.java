/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service.impl;

import com.google.common.base.Optional;
import cz.fai.utb.lang.api.LanguageProcessor;
import cz.sohlich.app.plugin.event.PluginLoadedEvent;
import cz.sohlich.app.service.PluginService;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author Radek
 */
@Component
public class PluginHolderImpl implements ApplicationListener, PluginService {

    @Value("${pg.plugin.path}")
    String pluginPath;

    static final HashMap<String, LanguageProcessor> plugins = new HashMap<>();

    @PostConstruct
    @Override
    public void load() {

        File loc = new File(pluginPath);

        //Add files with filter
        File[] flist = loc.listFiles((File file) -> file.getPath().toLowerCase().endsWith(".jar"));
        
        //return if there are no files to load
        if (flist == null)return;

        List<JarFile> jars = new ArrayList<>();
        for (File jar : flist) {
            try {
                jars.add(new JarFile(jar));
            } catch (IOException ex) {
                Logger.getLogger(PluginService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Files to Url
        URL[] urls;
        try {
            urls = FileUtils.toURLs(flist);
        } catch (IOException ex) {
            urls = new URL[0];
            Logger.getLogger(PluginService.class.getName()).log(Level.SEVERE, null, ex);
        }

        URLClassLoader ucl = URLClassLoader.newInstance(urls, Thread.currentThread().getContextClassLoader());

        jars.stream().map((jar) -> jar.entries()).forEach((e) -> {
            while (e.hasMoreElements()) {
                try {
                    JarEntry je = (JarEntry) e.nextElement();
                    if (je.isDirectory() || !je.getName().endsWith(".class")) {
                        continue;
                    }
                    String className = je.getName().replace(".class", "");
                    className = className.replace('/', '.');
                    ucl.loadClass(className);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(PluginService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        //Instantiate class for LanguageProcessor
        ServiceLoader<LanguageProcessor> sl = ServiceLoader.load(LanguageProcessor.class, ucl);
        Iterator<LanguageProcessor> apit = sl.iterator();
        while (apit.hasNext()) {
            LanguageProcessor plugin = apit.next();
            System.out.println("Loading: " + plugin.getClass().getName());
            plugins.put(plugin.getLang().toUpperCase(), plugin);
        }

        try {
            ucl.close();
        } catch (IOException ex) {
            Logger.getLogger(PluginService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public LanguageProcessor getProcessorByLanguage(String lang) {

        return plugins.get(lang.toUpperCase());

    }

    @Override
    public String[] extensionByLanguage(String lang) {
        LanguageProcessor processor = plugins.get(lang);
        return processor != null ? processor.getExtensions() : new String[0];
    }

    @Override
    public Set getSupportedLangs() {
        return plugins.keySet();
    }

    @Override
    public boolean isSupportedLang(String lang) {
        return plugins.containsKey(lang.toUpperCase());
    }

    @Override
    public void onApplicationEvent(ApplicationEvent e) {
        if (e instanceof PluginLoadedEvent) {
            this.load();
        }
    }

}
