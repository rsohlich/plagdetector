/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.util;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Radek
 */
public class FileHandler {

    String identificator;
    String configPath;
    String tempPath;

    public FileHandler(String configPath, String tempPath, String identificator) {
        this.configPath = configPath;
        this.tempPath = tempPath;
        this.identificator = identificator;
    }

    public FileHandler() {
        this(null, null, UUID.randomUUID().toString());
    }

    public FileHandler(String identificator) {
        this(null, null, identificator);
    }

    public FileHandler(String configPath, String tempPath) {
        this(configPath, tempPath, UUID.randomUUID().toString());
    }

    /**
     * Save file to temp directory
     *
     * @param file content of file
     * @return identificator under which the file is stored
     * @throws java.io.IOException
     */
    public String saveFile(byte[] file) throws IOException {
        //Build file path
        File uploadedFile = buildTempFilePath(identificator);
        FileUtils.writeByteArrayToFile(uploadedFile, file);
        return identificator;
    }

    public void deleteFromStorage() {
        File uploadedFile = buildTempFilePath(identificator);
        FileUtils.deleteQuietly(uploadedFile);
    }

    public File unzipToStorage() throws IOException {
        File uploadedFile = buildTempFilePath(identificator);
        String destPath = configPath + identificator;
        FileUtils.forceMkdir(new File(destPath));
        final File destination = new File(destPath);
        UnzipUtility.unzip(uploadedFile, destination);
        return destination;
    }

    private File buildTempFilePath(String identificator) {
        String path = tempPath + identificator + ".zip";
        return new File(path);
    }

    public void renameTempFile(String newIdentifier) throws IOException {
        FileUtils.moveFile(buildTempFilePath(identificator), buildTempFilePath(newIdentifier));
        identificator = newIdentifier;
    }

    public void cleanUp() throws IOException {
        FileUtils.deleteDirectory(new File(configPath + identificator));
    }
}
