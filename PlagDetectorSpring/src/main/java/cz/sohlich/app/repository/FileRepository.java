/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository;

import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dao.File;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Radek
 */
@Repository
public interface FileRepository extends MongoRepository<File, String>,FileRepositoryCustom{

    public List<File> findByOwner(String username);
    
    public List<File> findBySubmissionId(String submissionId);
    
    @Query("{ _id: { $in: ?0 } }")
    public List<File> findByIds(List<String> ids);
}
