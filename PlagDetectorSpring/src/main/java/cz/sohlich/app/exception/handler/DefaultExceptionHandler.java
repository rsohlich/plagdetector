/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.exception.handler;

import cz.sohlich.app.exception.LanguageUnsupportedException;
import cz.sohlich.app.exception.AssignmentNotFoundException;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author Radek
 */
@ControllerAdvice
public class DefaultExceptionHandler {
    Logger log = LoggerFactory.getLogger(DefaultExceptionHandler.class);
    
    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handleIOException(IOException ex) {
        
        log.error("Global exception", ex);
        // prepare responseEntity
        return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<String> handleNullPointerException(NullPointerException ex) {
        
        log.error("Global exception", ex);
        // prepare responseEntity
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
