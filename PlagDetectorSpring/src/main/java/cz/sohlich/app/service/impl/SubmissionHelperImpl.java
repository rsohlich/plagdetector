/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service.impl;

import cz.fai.utb.algorithms.FingerPrint;
import cz.fai.utb.algorithms.FingerPrintAlgorithm;
import cz.fai.utb.algorithms.winnowing.Winnowing;
import cz.fai.utb.lang.api.LanguageProcessor;
import cz.fai.utb.lang.api.ParseResultWrapper;
import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dao.File;
import cz.sohlich.app.repository.FileRepository;
import cz.sohlich.app.service.PluginService;
import cz.sohlich.app.service.SubmissionHelper;
import cz.sohlich.util.FileHandler;
import cz.sohlich.util.PatternUtility;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author radek
 */
@Service
public class SubmissionHelperImpl implements SubmissionHelper {

    @Autowired FileRepository fileRepo;
    @Autowired PluginService pluginHolder;

    @Override
    public void process(FileHandler fHandler, Assignment assignment, String owner, String submission) throws IOException {

        String submissionId;
        if (submission == null) {
            submissionId = UUID.randomUUID().toString();
            fHandler.renameTempFile(submissionId);
        } else {
            submissionId = submission;
        }

        //Unzip files to dest path
        java.io.File destination = fHandler.unzipToStorage();

        Collection<java.io.File> fileList
                = FileUtils.listFiles(destination, PatternUtility.extensionByLanguage(assignment.getLang()), true);

        //Process file
        LanguageProcessor processor = pluginHolder.getProcessorByLanguage(assignment.getLang());
        FingerPrintAlgorithm alg = new Winnowing();

        for (java.io.File fileItem : fileList) {
            String content = FileUtils.readFileToString(fileItem);
            ParseResultWrapper parseResult = processor.parseSource(content);
            List<String> doc = parseResult.getnGrams();
            String[] nGrams = doc.toArray(new String[0]);
            FingerPrint fingerPrint = alg.processTokensToFingerPrint(nGrams);

            File mongoFile = new File();
            mongoFile.setAssignment(assignment.getAssignmentId());
            mongoFile.setOwner(owner);
            mongoFile.setFilename(fileItem.getName());
            mongoFile.setSubmissionId(submissionId);
            mongoFile.setFile(content.getBytes());
            mongoFile.setFootprint(fingerPrint.getFingerMap());
            mongoFile.setSequence(fingerPrint.getFingerPrint());
            mongoFile.setIndexSequence(fingerPrint.getIndexFingerPrint());
            mongoFile.setClusterAttributes(ArrayUtils.add(parseResult.getMetrics(), content.length()));
            fileRepo.save(mongoFile);
        }

        fHandler.cleanUp();

    }

}
