/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author radek
 */
@ResponseStatus(value=HttpStatus.NOT_IMPLEMENTED, reason="Language of assignment not supported")
public class LanguageUnsupportedException extends Exception{
    
}
