/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.plugin.event;

import org.springframework.context.ApplicationEvent;

/**
 *
 * @author radek
 */
public class PluginLoadedEvent extends ApplicationEvent{

    public PluginLoadedEvent(Object source) {
        super(source);
    }
    
}
