/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.util.constant;

/**
 *
 * @author Radek
 */
public abstract class LangFileExtension {
    public static final String[] JAVA = {"java"};
    public static final String[] C = {"cpp","c"};
    public static final String[] NONE = {""};
}
