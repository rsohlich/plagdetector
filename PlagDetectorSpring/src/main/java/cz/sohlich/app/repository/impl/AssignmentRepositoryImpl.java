/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository.impl;

import cz.sohlich.app.entity.dto.DashboardStatItemDTO;
import cz.sohlich.app.repository.AssignmentRepositoryCustom;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 *
 * @author Radek
 */
public class AssignmentRepositoryImpl implements AssignmentRepositoryCustom {

    @Autowired
    MongoTemplate operations;

    @Override
    public List<DashboardStatItemDTO> getStatiscticsByUserAndDate(String owner) {
        Aggregation agg = newAggregation(
                project("owner", "createdOn").and("createdOn").extractDayOfYear().as("dayOfYear").and("createdOn").extractYear().as("year"),
                match(where("owner").is(owner)),
                group("owner", "dayOfYear","year").count().as("count")
        );

        AggregationResults<DashboardStatItemDTO> results = operations.aggregate(agg, "assignment", DashboardStatItemDTO.class);

        return results.getMappedResults();
    }
    
    
    
    

}
