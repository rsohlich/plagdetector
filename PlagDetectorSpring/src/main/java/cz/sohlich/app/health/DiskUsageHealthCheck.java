/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.health;

import com.codahale.metrics.health.HealthCheck;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author radek
 */
public class DiskUsageHealthCheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {
        Result result;
        File root = File.listRoots()[0];
        long totalSpace = root.getTotalSpace();
        long freeSpace = root.getFreeSpace();

        Map infoMap = new HashMap();
        infoMap.put("free space", freeSpace);
        infoMap.put("total space", totalSpace);

        if ((float)freeSpace / (float)totalSpace < 0.20) {

            result = Result.unhealthy("{'freeSpace':%f,'totalSpace':%f}", freeSpace/10e6,totalSpace/10e6);
            
        } else {

            result = Result.healthy("{'freeSpace':%f,'totalSpace':%f}", freeSpace/10e6,totalSpace/10e6);
        }
        return result;
    }

}
